package cn.spream.jstudy.ssi.manager.user.impl;

import cn.spream.jstudy.ssi.dao.user.UserDao;
import cn.spream.jstudy.ssi.domain.user.User;
import cn.spream.jstudy.ssi.manager.BaseManager;
import cn.spream.jstudy.ssi.manager.user.UserManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

/**
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-5-20
 * Time: 下午1:01
 * To change this template use File | Settings | File Templates.
 */
public class UserManagerImpl extends BaseManager implements UserManager {

    private UserDao userDao;

    @Override
    public boolean update(final User user) {
        boolean updated = (Boolean)getTransactionManager().execute(new TransactionCallback() {
            @Override
            public Object doInTransaction(TransactionStatus status) {
                return userDao.update(user);
//                status.setRollbackOnly();
            }
        });
        return updated;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
