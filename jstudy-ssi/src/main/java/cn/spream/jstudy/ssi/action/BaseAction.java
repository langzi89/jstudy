package cn.spream.jstudy.ssi.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.util.ValueStack;
import cn.spream.jstudy.ssi.common.Result;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * User: sjx
 * Date: 13-5-16
 * Time: 中午12:58
 * To change this template use File | Settings | File Templates.
 */
public class BaseAction extends ActionSupport implements ServletRequestAware, ServletResponseAware {

    protected int pageIndex = 1;
    protected final int pageSize = 2;
    
    protected HttpServletResponse response;
    protected HttpServletRequest request;

    /**
     * 将值写入
     * @param result
     */
    protected void toVm(Result result) {
        ValueStack context = ActionContext.getContext().getValueStack();
        context.set("result", result);
        Set<String> set = result.keySet();
        for (String key : set) {
            context.set(key, result.get(key));
        }
        String resultCode = result.getResultCode();
        if (StringUtils.isNotBlank(resultCode)) {
            String text;
            String[] params = result.getResultCodeParams();
            if (params != null && params.length > 0) {
                text = getText(resultCode, params);
            } else {
                text = getText(resultCode);
            }
            if (result.getSuccess()) {
                addActionMessage(text);
            } else {
                addActionError(text);
            }
        }
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    @Override
    public void setServletRequest(javax.servlet.http.HttpServletRequest httpServletRequest) {
        this.request = httpServletRequest;
    }

    @Override
    public void setServletResponse(javax.servlet.http.HttpServletResponse httpServletResponse) {
        this.response = httpServletResponse;
    }
}
