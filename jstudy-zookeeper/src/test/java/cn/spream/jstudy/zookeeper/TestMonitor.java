package cn.spream.jstudy.zookeeper;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by sjx on 15/11/16.
 */
public class TestMonitor {

    @Test
    public void test() throws IOException, InterruptedException {
        Process process = null;
        InputStream inputStream = null;
        String commands[] = {"/bin/sh", "-c", "echo mntr | nc 127.0.0.1 2181"};
        String result = null;
        try {
            process = Runtime.getRuntime().exec(commands);
            inputStream = process.getInputStream();
            result = IOUtils.toString(inputStream);
            process.waitFor();
        } finally {
            IOUtils.closeQuietly(inputStream);
            process.destroy();
        }
        System.out.println(result);
    }

}
