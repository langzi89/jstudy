package cn.spream.jstudy.netty;

/**
 * Created by sjx on 2015/11/6.
 */
public class Message {

    private long id;
    private byte[] key;
    private byte[] value;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public byte[] getKey() {
        return key;
    }

    public void setKey(byte[] key) {
        this.key = key;
    }

    public byte[] getValue() {
        return value;
    }

    public void setValue(byte[] value) {
        this.value = value;
    }
}
