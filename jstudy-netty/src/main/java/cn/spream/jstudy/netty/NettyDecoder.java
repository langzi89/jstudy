package cn.spream.jstudy.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * Created by sjx on 2015/11/6.
 */
public class NettyDecoder extends ByteToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        long id = in.readLong();
        int klen = in.readInt();
        byte[] key = new byte[klen];
        in.readBytes(key);
        int vlen = in.readInt();
        byte[] value = new byte[vlen];
        in.readBytes(value);
        Message message = new Message();
        message.setId(id);
        message.setKey(key);
        message.setValue(value);
        out.add(message);
    }
}
