package cn.spream.jstudy.ehcache;

import junit.framework.Assert;
import org.junit.Test;
import org.springframework.cache.Cache;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import cn.spream.jstudy.ehcache.domain.user.User;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 14-4-1
 * Time: 下午3:50
 * To change this template use File | Settings | File Templates.
 */
public class TestEhcache {

    private static Cache userCache;

    static {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        EhCacheCacheManager ehCacheCacheManager = (EhCacheCacheManager) context.getBean("ehCacheCacheManager");
        userCache = ehCacheCacheManager.getCache("userCache");
    }

    @Test
    public void testPut() {
        User user = new User();
        user.setId(1);
        user.setName("张三");
        String key = "user.id." + user.getId();
        userCache.put(key, user);
        User cacheUser = (User) userCache.get(key).get();
        Assert.assertTrue(cacheUser.getName().equals(user.getName()));
    }

}
