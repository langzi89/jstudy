package cn.spream.jstudy.designpattern.builder;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-7
 * Time: 上午10:57
 * To change this template use File | Settings | File Templates.
 */
public class TestComputerBuilder {

    @Test
    public void test() {
        ComputerBuilder computerBuilder = new ComputerBuilder();
        Computer dellComputer = computerBuilder.buildDellComputer();
        Computer asusComputer = computerBuilder.buildAsusComputer();
        Computer assembleComputer = computerBuilder.buildAssembleComputer();
        System.out.println(getConfiguration(dellComputer));
        System.out.println(getConfiguration(asusComputer));
        System.out.println(getConfiguration(assembleComputer));
    }

    private String getConfiguration(Computer computer) {
        return String.format("%s电脑配置(显示器：%s,主机：%s,键盘：%s,鼠标：%s)", computer.getName(), computer.getDisplay().getName(),
                computer.getCrate().getName(), computer.getKeyboard().getName(), computer.getMouse().getName());
    }

}
