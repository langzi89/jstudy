package cn.spream.jstudy.designpattern.factorymethod;

import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-6
 * Time: 下午1:58
 * To change this template use File | Settings | File Templates.
 */
public class TestSenderFactory {

    @Test
    public void send() {
        String info = "Hello Factory Method";
        Sender sender = SenderFactory.produce(SenderEnum.MAIL);
//        Sender sender = SenderFactory.produce(SenderEnum.SMS);
        sender.send(info);
    }

}
