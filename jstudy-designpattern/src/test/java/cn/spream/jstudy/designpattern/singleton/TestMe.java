package cn.spream.jstudy.designpattern.singleton;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-6
 * Time: 下午2:58
 * To change this template use File | Settings | File Templates.
 */
public class TestMe {

    private ExecutorService executorService;

    @Before
    public void before() {
        executorService = new ThreadPoolExecutor(10, 30, 10, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(365));
    }

    @Test
    public void test() throws InterruptedException {
        for (int i = 1; i <= 365; i++) {
            final int day = i;
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    Me me = Me.getInstance();
                    System.out.println("第" + day + "天我还是原来的我：" + me.getId());
                }
            });
        }
        TimeUnit.SECONDS.sleep(20);
    }

    @After
    public void after() {
        executorService.shutdown();
    }

}
