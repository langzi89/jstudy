package cn.spream.jstudy.designpattern.proxy;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 下午1:53
 * To change this template use File | Settings | File Templates.
 */
public class ProxyLog4j implements Log{

    private Log log = new Log4j();

    @Override
    public void debug(String debug) {
        System.out.println("ProxyLog4j.debug：before");
        log.debug(debug);
        System.out.println("ProxyLog4j.debug：after");
    }

    @Override
    public void info(String info) {
        System.out.println("ProxyLog4j.info：before");
        log.info(info);
        System.out.println("ProxyLog4j.info：after");
    }

    @Override
    public void error(String error) {
        System.out.println("ProxyLog4j.error：before");
        log.error(error);
        System.out.println("ProxyLog4j.error：after");
    }
}
