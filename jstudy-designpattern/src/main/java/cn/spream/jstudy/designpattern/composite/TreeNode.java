package cn.spream.jstudy.designpattern.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 下午4:24
 * To change this template use File | Settings | File Templates.
 */
public class TreeNode {

    private String name;
    private TreeNode parent;
    private List<TreeNode> childs = new ArrayList<TreeNode>();

    public TreeNode(String name) {
        this.name = name;
    }

    public boolean addChild(TreeNode treeNode){
        return childs.add(treeNode);
    }

    public boolean removeChild(TreeNode treeNode){
        return childs.remove(treeNode);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TreeNode getParent() {
        return parent;
    }

    public void setParent(TreeNode parent) {
        this.parent = parent;
    }

    public List<TreeNode> getChilds() {
        return childs;
    }

    public void setChilds(List<TreeNode> childs) {
        this.childs = childs;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof TreeNode)) return false;

        TreeNode treeNode = (TreeNode) object;

        if (!name.equals(treeNode.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
