package cn.spream.jstudy.designpattern.abstractfactory;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-6
 * Time: 下午1:52
 * To change this template use File | Settings | File Templates.
 */
public interface SenderFactory {

    public Sender produce();

}
