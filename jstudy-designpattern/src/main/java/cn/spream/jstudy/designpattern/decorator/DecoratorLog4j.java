package cn.spream.jstudy.designpattern.decorator;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 上午11:11
 * To change this template use File | Settings | File Templates.
 */
public class DecoratorLog4j implements Log {

    private Log4j log4j;

    public DecoratorLog4j(Log4j log4j) {
        this.log4j = log4j;
    }

    @Override
    public void debug(String debug) {
        System.out.println("DecoratorLog4j.debug：before");
        log4j.debug(debug);
        System.out.println("DecoratorLog4j.debug：after");
    }

    @Override
    public void info(String info) {
        System.out.println("DecoratorLog4j.info：before");
        log4j.info(info);
        System.out.println("DecoratorLog4j.info：after");
    }

    @Override
    public void error(String error) {
        System.out.println("DecoratorLog4j.error：before");
        log4j.error(error);
        System.out.println("DecoratorLog4j.error：after");
    }
}
