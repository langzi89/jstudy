package cn.spream.jstudy.designpattern.singleton;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-6
 * Time: 下午2:51
 * To change this template use File | Settings | File Templates.
 */
public class Me {

    //身份证id
    private long id;

    /**
     * 私有构造，防止实例化
     */
    private Me() {
        id = new Date().getTime();
    }

    /**
     * 静态内部类实例化对象
     */
    private static class MeFactory {
        private static Me me = new Me();
    }

    /**
     * 获取单例对象
     *
     * @return
     */
    public static Me getInstance() {
        return MeFactory.me;
    }

    /**
     * 序列化调用
     *
     * @return
     */
    public Object readResolve() {
        return getInstance();
    }

    public long getId() {
        return id;
    }
}
