package cn.spream.jstudy.designpattern.command;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 下午2:35
 * To change this template use File | Settings | File Templates.
 */
public interface Command {

    public void execute();

}
