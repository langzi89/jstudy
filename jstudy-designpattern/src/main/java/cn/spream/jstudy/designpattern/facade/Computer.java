package cn.spream.jstudy.designpattern.facade;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-8
 * Time: 下午2:37
 * To change this template use File | Settings | File Templates.
 */
public class Computer {

    private CPU cpu;
    private Memory memory;
    private OS os;

    public Computer() {
        cpu = new CPU();
        memory = new Memory();
        os = new OS();
    }

    public void startup() {
        cpu.startup();
        memory.startup();
        os.startup();
        System.out.println("电脑启动");
    }

    public void shutdown() {
        os.shutdown();
        cpu.shutdown();
        os.shutdown();
        System.out.println("电脑停止");
    }

}
