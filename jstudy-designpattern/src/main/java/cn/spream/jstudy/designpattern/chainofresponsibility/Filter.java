package cn.spream.jstudy.designpattern.chainofresponsibility;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15-1-16
 * Time: 下午1:50
 * To change this template use File | Settings | File Templates.
 */
public interface Filter {

    public void doFilter(Request request, Response response, FilterChain chain);

}
