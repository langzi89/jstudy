package cn.spream.jstudy.designpattern.mediator;

/**
 * Created with IntelliJ IDEA.
 * User: sjx
 * Date: 15/1/18
 * Time: 下午3:24
 * To change this template use File | Settings | File Templates.
 */
public abstract class Colleague {

    protected int number;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public abstract void setNumber(int number, Mediator mediator);

}
